package com.example.aninterface;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;

import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity2 extends AppCompatActivity {
            private Button button13;

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main2);

                BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
                bottomNavigationView.setOnNavigationItemSelectedListener(navListener);

            }
    private BottomNavigationView.OnNavigationItemSelectedListener navListener=new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectedFragment = null;
            switch (menuItem.getItemId()) {

                case R.id.notification:
                    selectedFragment = new notification();
                    break;
                case R.id.emergency:
                    selectedFragment = new emergencyfrag();
                    break;
                case R.id.navhome:
                    selectedFragment = new homefragment();
                    break;
                case R.id.profile:
                    selectedFragment = new profilefrag();
                    break;
                case R.id.navsetting:
                    selectedFragment = new settingsfrag();
                    break;            }
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                return true;
            }
};
}
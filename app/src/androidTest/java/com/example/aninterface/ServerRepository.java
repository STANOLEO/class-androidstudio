package com.example.aninterface;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
      public class ServerRepository{
        private IServerWService ws;
        public ServerRepository(){
            Retrofit retro = new Retrofit.Builder()
                    .baseUrl("https://hilkroftinsurance.com:8445")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            this.ws=retro.create(IServerWService.class);
        }
        public void  getStatus(){
            this.ws.getStatus().enqueue(new Callback<server>() {
                @Override
                public void onResponse(Call<server> call, Response<server> response) {
                  Server s = response.body();
                  Log.e("SRES",s.getStatus());
                }

                @Override
                public void onFailure(Call<server> call, Throwable t) {

                }

            });
        }
    }


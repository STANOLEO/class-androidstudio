package com.example.aninterface;

public class server {
    private String Status;
    private String Group;
    private String Version;
    private String Artifact;

    public String getStatus() {
        return Status;
    }

    public String getGroup() {
        return Group;
    }

    public String getVersion() {
        return Version;
    }

    public String Artifact() {
        return Artifact;
    }

    public void setArtifact(String artifact) {
        Artifact = artifact;
    }

    public void setGroup(String Group) {
        Group = Group;
    }

    public void setVersion(String Version) {
        Version = Version;
    }

}
package com.example.aninterface;

import retrofit2.Call;
import retrofit2.http.GET;

public interface IServerWService {
    @GET("/")
    Call<server> getStatus();
}
